#CC=gcc
CC=i686-w64-mingw32-gcc
OS=32
FIND_TOOL=find

SYS := $(shell $(CC) -dumpmachine)
#ifneq (, $(findstring linux, $(SYS)))
 FIND_TOOL = find
#else ifneq (, $(findstring mingw, $(SYS)))
# FIND_TOOL = /bin/find
#endif

BITS := $(shell uname -m)
ifeq ($(BITS), x86_64)
 OS = 64
endif

LDFLAGS = -Llib -lm
WIN32_MINGW_LDFLAGS = -lmingw32win lib/SDL_image.lib -lSDLmain -lSDL.dll -lglu32 -lglew32.dll -lopengl32 -lmatrix4_mingw
LINUX_MINGW_LDFLAGS = -lmingw32 lib/SDL_image.lib -lSDLmain -lSDL.dll -lglu32 -lglew32.dll -lopengl32 -lmatrix4_mingw
LINUX_LDFLAGS = -lSDL -lSDL_image -lGL -lGLU -lGLEW -lmatrix4_$(OS)

CFLAGS = -g
SRCS = $(shell $(FIND_TOOL) ./src -type f -name "*.c" |tr "\\n" " ")
OBS = $(shell $(FIND_TOOL) ./src -type f -name "*.c" |sed "s/\.c/.o/g" |tr "\\n" " ")

all: windows

windows: $(OBS)
	$(CC) $(OBS) $(LDFLAGS) $(WIN32_MINGW_LDFLAGS) -o ./bin/grid_win$(OS)

linux: $(OBS)
	$(CC) $(OBS) $(LDFLAGS) $(LINUX_LDFLAGS) -o ./bin/grid_linux$(OS)

linux-mingw: $(OBS)
	$(CC) $(OBS) $(LDFLAGS) $(LINUX_MINGW_LDFLAGS) -o ./bin/grid_win$(OS)
	
%.o: %.c
	$(CC) $(CFLAGS) -c -Iinclude $< -o $@

clean:
	@echo $(OBS) |xargs rm -f
	
test:
	echo $(OBS)
