#ifndef GL_H
#define GL_H

#include <GL/glew.h>
#include "types.h"
#include "matrix4.h"

#define SCREEN_WIDTH 1280
#define SCREEN_HEIGHT 800
#define FOV 80.0f
#define ZNEAR 0.1f
#define ZFAR 2000.0f

#define COLOR_BLACK 0.f, 0.f, 0.f
#define COLOR_RED   1.f, 0.f, 0.f
#define COLOR_WHITE 1.f, 1.f, 1.f

#define VERT_SHADER "./shader.vert"
#define FRAG_SHADER "./shader.frag"

#define SWAP_ROWS_FLOAT(a, b) { float *_tmp = a; (a)=(b); (b)=_tmp; }

typedef struct {
	GLuint fbo, rb, texture, texture2, depth;
} FramebufferData;

typedef struct {
	GLuint program;
	GLint viewMatrixLoc, projMatrixLoc, modelMatrixLoc;
	Matrix4f viewMatrix, projMatrix;
} ShaderData;

int glhProjectf(float objX, float objY, float objZ, const Matrix4f *viewMatrix, const Matrix4f *projMatrix, float *windowCoordinate);
int glhUnProjectf(float x, float y, const Matrix4f *viewMatrix, const Matrix4f *projMatrix, float *outXYZ);
int glhInvertMatrixf2(const Matrix4f *mat, Matrix4f *out);
ShaderData loadShader(const char* vertShaderPath, const char* fragShaderPath);
GLuint createProgram(const char* vertShaderPath, const char* fragShaderPath, ShaderData *data);
GLuint createShader(GLenum shaderType, const char* path);
void deleteShaderObject(GLuint program, GLuint shader);
void deleteProgram(ShaderData *data);
void checkShader(const char* what, GLuint shader);
void setViewMatrix(ShaderData *data, float cposX, float cposY, float cposZ, float crotX, float crotY, float crotZ);
int enableShader(const ShaderData *data);
void disableShader();
void setModelMatrix(const Matrix4f *matrix, const ShaderData *data);
void setProjectionMatrix(ShaderData *data, float fov, float zNear, float zFar);
void setOrthoProjectionMatrix(ShaderData *data, float left, float right, float bottom, float top, float zNear, float zFar);
int createFramebuffer(FramebufferData *data);

bool initGL(void);
inline float degToRad(float deg);

static const GLfloat triangleData[] = {
	-1.0f, -1.0f, 0.0f,
	 1.0f, -1.0f, 0.0f,
	 0.0f,  1.0f, 0.0f
};

#endif
