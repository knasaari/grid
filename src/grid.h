#ifndef GRID_H
#define GRID_H

#include <GL/glew.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
	char type;
	char light;
} Block;

typedef struct {
	GLfloat x,y,z,u,v;
} Vertex;

typedef struct {
	int width;
	int height;
	int worldWidth;
	int worldHeight;
	int gridSize;
	int VBOindexcount;
} Grid;

typedef struct {
	GLuint id;
	int indexCount;
	int vertexSize;
	Vertex *data;
	int *chunkSizes;
} VBO;

typedef struct {
	float u0, u1, v0, v1;
} UV4;

void initUV4(UV4 **uvs, GLuint texture);
int initGrid(Grid *grid, Block ****grid_data, int worldWidth, int worldHeight, int gridSize);
void generateGrid(Grid *grid, Block ****grid_p);
void createVBO(Grid *grid, VBO *vbo);
void updateVBO(Grid *grid, Block ***grid_p, VBO *vbo, UV4 *uv);
int addBlock(Vertex* array, int *it, int offsetX, int offsetY, int offsetZ, int *others, const UV4 *t0, const UV4 *t1);
int blockAt(Block ***grid, Grid *g, int x, int y, int z);
int whichChunk(Grid *grid, int x, int y, int z);
int setBlock(Grid *g, Block ***grid_data, int x, int y, int z, int type);
void updateChunk(Grid *grid, Block ***grid_data, VBO *vbo, UV4 *uv, int chunkId);
inline float findnoise2(float x,float y);
inline float interpolate(float a,float b,float x);
float noise(float x,float y);
#endif
