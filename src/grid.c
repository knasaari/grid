#include "grid.h"
#include <math.h>
#include <string.h>
/**
 * Perlin noise by "Pontus"
 * Original code: http://www.dreamincode.net/forums/topic/66480-perlin-noise/
 */
 
int seed = 0;
float pinr = 1;
float pinc = 0;

void calculateUV4(UV4 *dest, float w, float h, int id, int tileSizeX, int tileSizeY) {
	float ufrac = tileSizeX / w;
	float vfrac = tileSizeY / h;
	int uTiles = (w / tileSizeX);
	//int vTiles = (h/tileSizeY);
	float col = id % uTiles;
	float row = floorf(id / uTiles);

	dest->u0 = ufrac*col;
	dest->v0 = vfrac*row;
	dest->u1 = dest->u0 + ufrac;
	dest->v1 = dest->v0 + vfrac;
}

void initUV4(UV4 **uvs, GLuint texture){
	*uvs = (UV4*)malloc(16*16*sizeof(UV4));
	int i;
	for(i=0; i<16*16; i++){
		calculateUV4((*uvs)+i, 512, 512, i, 32, 32);
	}
}

int initGrid(Grid *grid, Block ****grid_data, int worldWidth, int worldHeight, int gridSize){
	grid->width = worldWidth*gridSize;
	grid->height = worldHeight*gridSize;
	grid->worldWidth = worldWidth;
	grid->worldHeight = worldHeight;
	grid->gridSize = gridSize;
	
	// Allocate z
	*grid_data = (Block***)malloc(grid->width * sizeof(Block *));
	int z,y,x;
	for(z = 0; z < grid->width; z++){
		// Allocate y
		(*grid_data)[z] = (Block**)malloc(grid->height * sizeof(Block *));
		for(y = 0; y < grid->height; y++){
			// Allocate x
			(*grid_data)[z][y] = (Block*)malloc(grid->width * sizeof(Block));
			if((*grid_data)[z][y] == NULL){
				printf("out of memory\n");
				return 0;
			}
		}
	}
	
	for(z=0; z < grid->width; z++){
		for(y=0; y < grid->height; y++){
			memset((*grid_data)[z][y], 0, grid->width * sizeof(Block));
		}
	}

	printf("Initialized grid %d*%d*%d\n", grid->width, grid->height, grid->width);	
	printf("Grid data size %f MB\n", (grid->width*grid->height*grid->width*sizeof(Block))/1024.0f/1024.0f);	
	return 1;
}

void generateGrid(Grid *grid, Block ****grid_p){
	//seed = rand()%1376312;
	seed++;

	Block ***grid_data = *grid_p;
	int x,y,d;
	int octaves = 3;
	float zoom = 35.0f;
	float range = 5.0f;
	float p = 2.5f;
	for(y=0; y<grid->width; y++){
		for(x=0; x<grid->width; x++){
			
			float getnoise = 0;
			// octaves
			int a;
			for(a = 0; a < octaves-1; a++){
				float frequency = pow(2,a);// This increases the frequency with every loop of the octave.
				float amplitude = pow(p,a);// This decreases the amplitude with every loop of the octave.
				getnoise += noise(((float)x)*frequency/zoom,((float)y)/zoom*frequency)*amplitude;//This uses our perlin noise functions. It calculates all our zoom and frequency and amplitude
			}// It gives a decimal value, you know, between the pixels. Like 4.2 or 5.1
			int color = (int)((getnoise*range)+range);
			if(color >= grid->height)
				color = grid->height-1;
			if(color < 0)
				color = 0;
			
			d = color;
			
			for(d; d >= 0; d--){
				grid_data[y][d][x].type = (d == color)?1:2;
			}
		}
	}
	y = grid->width/2;
	x = y;
	d = grid->height-1;
	for(d; d >= 0; d--){
		if(grid_data[y][d][x].type == 1){
			grid_data[y][d][x].light = 15;
			break;
		}
	}
	
	printf("Grid generated\n");
	printf("%d,%d light %d\n", x, y, grid_data[y][d][x].light);
}

void createVBO(Grid *grid, VBO *vbo){
	vbo->vertexSize = 3+2; // XYZ,UV
	vbo->indexCount = (grid->width * grid->height * grid->width * 6 * 2 * 3);
	vbo->indexCount /= 18;
	vbo->data = (Vertex*)malloc(vbo->indexCount*sizeof(Vertex));
	vbo->chunkSizes = (int *)malloc(grid->worldWidth*grid->worldHeight*grid->worldWidth*sizeof(int));
	
	glGenBuffers(1, &(vbo->id));
	glBindBuffer(GL_ARRAY_BUFFER, vbo->id);
	glBufferData(GL_ARRAY_BUFFER, vbo->indexCount*sizeof(Vertex), 0, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	printf("VBO created. VBO floats %d. VBO indices %d.\n", vbo->indexCount*vbo->vertexSize, vbo->indexCount);
}

void updateVBO(Grid *grid, Block ***grid_data, VBO *vbo, UV4 *uv){
	int others[6];
	int verticesInOneBlock = 6*2*3; // 6sides * 2tris * 3indices * vbo->vertexSize
	// Chunk max size (floats)
	int oneChunkMaxSize = grid->gridSize*grid->gridSize*grid->gridSize*verticesInOneBlock;
	// Decrease chunk max size, since we will never really have all blocks with all sides
	oneChunkMaxSize /= 18;
	
	int b;
	for(b=0; b<6; b++){
		others[b] = 0;
	}

	int wx,wy,wz,gx,gy,gz,curGx,curGz,curGy,i=0,currentChunk,uvId=0;
	UV4 *topBottomUV, *sidesUV;
	for(wz=0; wz<grid->worldWidth; wz++){
		for(wy=0; wy<grid->worldHeight; wy++){
			for(wx=0; wx<grid->worldWidth; wx++){
				currentChunk = wz*grid->worldHeight*grid->worldWidth + wy*grid->worldWidth + wx;
				i = (oneChunkMaxSize*currentChunk); // Set vbo->data iterator to the beginning of this chunk
				int currentChunkSize = 0; // Current chunk size (floats)
				
				for(gz=0; gz<grid->gridSize; gz++){
					for(gy=0; gy<grid->gridSize; gy++){
						for(gx=0; gx<grid->gridSize; gx++){
							curGz = wz * grid->gridSize+gz;
							curGy = wy * grid->gridSize+gy;
							curGx = wx * grid->gridSize+gx;

							if(grid_data[curGz][curGy][curGx].type == 0)
								continue;
							// Don't go over the decreased chunk size limit
							if(currentChunkSize+verticesInOneBlock > oneChunkMaxSize)
								continue;
								
							others[0] = blockAt(grid_data, grid, curGx-1, curGy, curGz);
							others[1] = blockAt(grid_data, grid, curGx+1, curGy, curGz);
							others[2] = blockAt(grid_data, grid, curGx, curGy-1, curGz);
							others[3] = blockAt(grid_data, grid, curGx, curGy+1, curGz);
							others[4] = blockAt(grid_data, grid, curGx, curGy, curGz-1);
							others[5] = blockAt(grid_data, grid, curGx, curGy, curGz+1);
							
                                                        if(curGy == 0){
								topBottomUV = uv+207;
								sidesUV = uv+207;
							}
							else if(grid_data[curGz][curGy][curGx].type == 2){
								topBottomUV = uv+1;
								sidesUV = uv+1;
							}
                                                        else{
                                                            topBottomUV = uv;
                                                            if(others[3])
                                                                sidesUV = uv+2;
                                                            else
                                                                sidesUV = uv+3;
                                                        }

							
							currentChunkSize += addBlock(vbo->data, &i, curGx, curGy, curGz, others, topBottomUV, sidesUV);
							for(b=0; b<6; b++){
								others[b] = 0;
							}
						}
					}
				}
				/*
				if(currentChunkSize > 0)
					printf("Chunk %d size %d\n",currentChunk, currentChunkSize/vbo->vertexSize/3);
				*/
				vbo->chunkSizes[currentChunk] = currentChunkSize;
			}
		}
	}
	int totalFloats = 0;
	for(i=0; i<grid->worldWidth*grid->worldHeight*grid->worldWidth;i++)
		totalFloats += vbo->chunkSizes[i]*vbo->vertexSize;
		
	glBindBuffer(GL_ARRAY_BUFFER, vbo->id);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vbo->indexCount*vbo->vertexSize*sizeof(GLfloat), vbo->data);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	printf("VBO updated. VBO total size %.2f MB, current usage %.2f MB\n",(float)(vbo->indexCount*vbo->vertexSize*4)/1024.0f/1024.0f, (float)totalFloats*4.0f/1024.0f/1024.0f);
	printf("Drawing %d triangles\n",totalFloats/vbo->vertexSize/3);
}

int whichChunk(Grid *grid, int x, int y, int z){
        int wx = (x-(x%grid->gridSize))/grid->gridSize;
        int wy = (y-(y%grid->gridSize))/grid->gridSize;
        int wz = (z-(z%grid->gridSize))/grid->gridSize;
        int id = wz*grid->worldHeight*grid->worldWidth + wy*grid->worldHeight + wx;
        printf("Got chunk %d\n",id);
        return id;
}

int setBlock(Grid *g, Block ***grid_data, int x, int y, int z, int type){
        if(x < 0 || z < 0 || y < 0 || x >= g->width || z >= g->width || y >= g->height)
            return 0;
        else
            grid_data[z][y][x].type = type;
        
        printf("set %d, %d, %d to %d\n",x,y,z,type);
        return 1;
}

void updateChunk(Grid *grid, Block ***grid_data, VBO *vbo, UV4 *uv, int chunkId){
        if(chunkId < 0 || chunkId >= grid->worldWidth*grid->worldHeight*grid->worldWidth){
            return;
        }
        
        int others[6];
	int verticesInOneBlock = 6*2*3; // 6sides * 2tris * 3indices
	// Chunk max size (floats)
	int oneChunkMaxSize = grid->gridSize*grid->gridSize*grid->gridSize*verticesInOneBlock;
	// Decrease chunk max size, since we will never really have all blocks with all sides
	oneChunkMaxSize /= 18;
        
        int b;
	for(b=0; b<6; b++){
		others[b] = 0;
	}
        
        int wx,wy,wz,gx,gy,gz,curGx,curGz,curGy,i=0,currentChunk = 0,uvId=0;
        UV4 *topBottomUV, *sidesUV;
        for(wz=0; wz<grid->worldWidth; wz++){
                for(wy=0; wy<grid->worldHeight; wy++){
                        for(wx=0; wx<grid->worldWidth; wx++){
                                if(currentChunk == chunkId){
                                    i = (oneChunkMaxSize*currentChunk); // Set vbo->data iterator to the beginning of this chunk
                                    int data_offset = i;
                                    int currentChunkSize = 0;

                                    for(gz=0; gz<grid->gridSize; gz++){
                                            for(gy=0; gy<grid->gridSize; gy++){
                                                    for(gx=0; gx<grid->gridSize; gx++){
                                                            curGz = wz * grid->gridSize+gz;
                                                            curGy = wy * grid->gridSize+gy;
                                                            curGx = wx * grid->gridSize+gx;

                                                            if(grid_data[curGz][curGy][curGx].type == 0)
                                                                    continue;
                                                            // Don't go over the decreased chunk size limit
                                                            if(currentChunkSize+verticesInOneBlock > oneChunkMaxSize)
                                                                    continue;

                                                            others[0] = blockAt(grid_data, grid, curGx-1, curGy, curGz);
                                                            others[1] = blockAt(grid_data, grid, curGx+1, curGy, curGz);
                                                            others[2] = blockAt(grid_data, grid, curGx, curGy-1, curGz);
                                                            others[3] = blockAt(grid_data, grid, curGx, curGy+1, curGz);
                                                            others[4] = blockAt(grid_data, grid, curGx, curGy, curGz-1);
                                                            others[5] = blockAt(grid_data, grid, curGx, curGy, curGz+1);
                                                            
                                                            if(curGy == 0){
								topBottomUV = uv+207;
								sidesUV = uv+207;
                                                            }
                                                            else if(grid_data[curGz][curGy][curGx].type == 2){
                                                                    topBottomUV = uv+1;
                                                                    sidesUV = uv+1;
                                                            }
                                                            else{
                                                                topBottomUV = uv;
                                                                if(others[3])
                                                                    sidesUV = uv+2;
                                                                else
                                                                    sidesUV = uv+3;
                                                            }
                                                            
                                                            currentChunkSize += addBlock(vbo->data, &i, curGx, curGy, curGz, others, topBottomUV, sidesUV);
                                                    }
                                            }
                                    }
                                    vbo->chunkSizes[currentChunk] = currentChunkSize;

                                    glBindBuffer(GL_ARRAY_BUFFER, vbo->id);
                                    glBufferSubData(GL_ARRAY_BUFFER, data_offset*vbo->vertexSize*sizeof(GLfloat), currentChunkSize*vbo->vertexSize*sizeof(GLfloat), vbo->data+data_offset);
                                    glBindBuffer(GL_ARRAY_BUFFER, 0);
                                    
                                    printf("Updated chunk %d\n",chunkId);
                                    return;
                                }
                                currentChunk++;
                        }
                }
        }
}

int blockAt(Block ***grid, Grid *g, int x, int y, int z) {
	if(y == -1)
		return 1;
	else
		return (x < 0 || z < 0 || y < 0 || x >= g->width || z >= g->width || y >= g->height)?0:grid[z][y][x].type;
}

int addBlock(Vertex* array, int *it, int offsetX, int offsetY, int offsetZ, int *others, const UV4 *t0, const UV4 *t1){
	int vertexCount = 0;
	int verticesPerSide = 6; // 2*3*(3+2)
	int i = *it;
	
	if(others[0] == 0){
		// Left side (-x)
		vertexCount += verticesPerSide;
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z =-0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v1;
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = 0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v1;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = 0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v0;
		
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v1;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v0;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v0;
	}
	
	if(others[1] == 0){
		// Right side (+x)
		vertexCount += verticesPerSide;
		array[i].x =  0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v0;
		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v1;
		array[i].x =  0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v0;

		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v1;
		array[i].x =  0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v0;
		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v1;
	}
	if(others[2] == 0){
		// Bottom side (-y)
		vertexCount += verticesPerSide;
		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t0->u0;	array[i++].v = t0->v1;
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t0->u1;	array[i++].v = t0->v0;
		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t0->u0;	array[i++].v = t0->v0;
		
		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t0->u0;	array[i++].v = t0->v1;
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t0->u1;	array[i++].v = t0->v1;
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t0->u1;	array[i++].v = t0->v0;
	}
	
	if(others[3] == 0){
		// Top side (+y)
		vertexCount += verticesPerSide;
		array[i].x =  0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t0->u1;	array[i++].v = t0->v1;
		array[i].x =  0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t0->u1;	array[i++].v = t0->v0;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t0->u0;	array[i++].v = t0->v0;

		array[i].x =  0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t0->u1;	array[i++].v = t0->v1;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t0->u0;	array[i++].v = t0->v0;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t0->u0;	array[i++].v = t0->v1;
	}
	
	if(others[4] == 0){
		// Back side (-z)
		vertexCount += verticesPerSide;
		array[i].x =  0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v0;
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v1;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v0;

		array[i].x =  0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v0;
		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v1;
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z = -0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v1;
	}
	
	if(others[5] == 0){
		// Front side (+z)
		vertexCount += verticesPerSide;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v0;
		array[i].x = -0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v1;
		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v1;

		array[i].x = 0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v0;
		array[i].x = -0.5+offsetX; array[i].y =  0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u0;	array[i++].v = t1->v0;
		array[i].x =  0.5+offsetX; array[i].y = -0.5+offsetY; array[i].z =  0.5+offsetZ;
		array[i].u = t1->u1;	array[i++].v = t1->v1;
	}

	*it = i;
	return vertexCount;
}


inline float findnoise2(float x,float y){
	int n=(int)x+(int)y*57;
	n=(n<<13)^n;
	int nn=(n*(n*n*60493+seed)+1376312589)&0x7fffffff;
	return 1.0f-((float)nn/1073741824.0);
}


float interpolate(float a,float b,float x){
	float ft=x * 3.1415927f;
	float f=(1.0f-(float)cos(ft))* 0.5f;
	return a*(1.0f-f)+b*f;
}

float noise(float x,float y){
	float floorx=(float)((int)x);
	float floory=(float)((int)y);
	float s,t,u,v;
	s=findnoise2(floorx,floory); 
	t=findnoise2(floorx+1,floory);
	u=findnoise2(floorx,floory+1);
	v=findnoise2(floorx+1,floory+1);
	float int1=interpolate(s,t,x-floorx);
	float int2=interpolate(u,v,x-floorx);
	return interpolate(int1,int2,y-floory);
}
