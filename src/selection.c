#include "selection.h"

void createSelection(VBO *vbo, UV4 *uv){
        vbo->vertexSize = 3+2; // XYZ,UV
        vbo->indexCount = (1 * 6 * 2 * 3);
        vbo->data = (Vertex*)malloc(vbo->indexCount*sizeof(Vertex));

        

        printf("VBO created. VBO floats %d. VBO indexes %d.\n", vbo->indexCount*vbo->vertexSize, vbo->indexCount);
        
        int others[6];
	int b;
	for(b=0; b<6; b++){
		others[b] = 0;
	}
        
        b = 0;
        
        addBlock(vbo->data, &b, 0, 0, 0, others, uv+239, uv+239);
        
        glGenBuffers(1, &(vbo->id));
        glBindBuffer(GL_ARRAY_BUFFER, vbo->id);
        glBufferData(GL_ARRAY_BUFFER, vbo->indexCount*sizeof(Vertex), vbo->data, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
}
