#include <stdio.h>
#include <stdlib.h>
#include "filereader.h"
#include <string.h>
#include <errno.h>

char* readFile(const char *filename) {
	char* buffer = NULL;
	int fileSize, readSize;

	FILE *handler = fopen(filename, "rb");

	if(handler) {
		fseek(handler, 0, SEEK_END);
		fileSize = ftell(handler);
		rewind(handler);

		buffer = (char*)malloc(sizeof(char)*(fileSize+1));

		readSize = fread(buffer, sizeof(char), fileSize, handler);

		buffer[fileSize] = '\0';
		printf("%i %i", fileSize, readSize);
		if(fileSize != readSize) {
			free(buffer);
			buffer = NULL;
			printf("couldn't read file: %s\n", filename);
		}
	}
	else
		printf("Couldn't open file %s: %s\n", filename, strerror(errno));

	fclose(handler);
	return buffer;
}
