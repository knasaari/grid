#include "types.h"

Vec3f TVec3f(float x, float y, float z) {
	Vec3f v;
	
	v.x = x;
	v.y = y;
	v.z = z; 
	
	return v;
}
