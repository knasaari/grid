#include <stdlib.h>
#include <stdio.h>
#include "gl.h"
#include "filereader.h"
#include <SDL/SDL_image.h>
#include <GL/glew.h>
#include <math.h>
#include <GL/glu.h>

bool initGL() {
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	 gluPerspective(80.0f,(GLfloat)SCREEN_WIDTH/(GLfloat)SCREEN_HEIGHT,0.1f,2000.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClearColor(COLOR_BLACK, 1.0f);

	glClearDepth(1.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glShadeModel(GL_FLAT);
	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	GLenum error = glGetError();
	if(error != GL_NO_ERROR)
		return false;

	glewInit();

	if(!glewIsSupported("GL_VERSION_2_0"))
		return false;

	return true;
}

int glhProjectf(float objX, float objY, float objZ, const Matrix4f *viewMatrix, const Matrix4f *projMatrix, float *windowCoordinate) {
	float obj[4];
	obj[0] = objX;
	obj[1] = objY;
	obj[2] = objZ;
	obj[3] = 1.0f;
	
	//Transformation vectors
	float fTempo[8];
	
	//Modelview transform
	transformMat4(viewMatrix, obj, fTempo);
	
	//Projection transform, the final row of projection matrix is always [0 0 -1 0]
	//so we optimize for that.
	transformMat4(projMatrix, fTempo, fTempo+4);
	fTempo[7] = -fTempo[2];
	
	//The result normalizes between -1 and 1
	if (fTempo[7] == 0.0) //The w value
		return 0;
	
	fTempo[7] = 1.0 / fTempo[7];
	
	//Perspective division
	fTempo[4] *= fTempo[7];
	fTempo[5] *= fTempo[7];
	fTempo[6] *= fTempo[7];
	
	GLint viewport[4];
	glGetIntegerv(GL_VIEWPORT, viewport);
	
	//Window coordinates
	//Map x, y to range 0-1
	windowCoordinate[0] = (fTempo[4]*0.5 + 0.5) * viewport[2] + viewport[0];
	windowCoordinate[1] = viewport[3] - ((fTempo[5]*0.5 + 0.5) * viewport[3] + viewport[1]);
	//This is only correct when glDepthRange(0.0, 1.0)
	windowCoordinate[2] = (1.0 + fTempo[6])*0.5; //Between 0 and 1
	
	return 1;
}

int glhUnProjectf(float x, float y, const Matrix4f *viewMatrix, const Matrix4f *projMatrix, float *outXYZ){
	Matrix4f mat,matInvert;
	GLint viewport[4];
	float z;
	float in[4], out[4];

	glGetIntegerv(GL_VIEWPORT, viewport);
	mulMat4(projMatrix, viewMatrix, &mat);
	
	if(glhInvertMatrixf2(&mat, &matInvert) == 0)
		return 0;
	
	y = ((float)viewport[3]) - y;
	glReadPixels((int)x, (int)y, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, (void*)(&z));
	
	//Transformation of normalized coordinates between -1 and 1
	in[0] = (x-(float)viewport[0])/(float)viewport[2]*2.0-1.0;
	in[1] = (y-(float)viewport[1])/(float)viewport[3]*2.0-1.0;
	in[2] = 2.0*z-1.0;
	in[3] = 1.0;
	
	//Objects coordinates
	transformMat4(&matInvert, in, out);
	
	if(out[3]==0.0)
		return 0;
	
	out[3]=1.0/out[3];
	outXYZ[0] = out[0]*out[3];
	outXYZ[1] = out[1]*out[3];
	outXYZ[2] = out[2]*out[3];
	
	return 1;
}

//This code comes directly from GLU except that it is for float
int glhInvertMatrixf2(const Matrix4f *mat, Matrix4f *out){
	float wtmp[4][8];
	float m0, m1, m2, m3, s;
	float *r0, *r1, *r2, *r3;

	r0 = wtmp[0], r1 = wtmp[1], r2 = wtmp[2], r3 = wtmp[3];

	r0[0] = mat->m00, r0[1] = mat->m01,
		r0[2] = mat->m02, r0[3] = mat->m03,
		r0[4] = 1.0, r0[5] = r0[6] = r0[7] = 0.0,
		r1[0] = mat->m10, r1[1] = mat->m11,
		r1[2] = mat->m12, r1[3] = mat->m13,
		r1[5] = 1.0, r1[4] = r1[6] = r1[7] = 0.0,
		r2[0] = mat->m20, r2[1] = mat->m21,
		r2[2] = mat->m22, r2[3] = mat->m23,
		r2[6] = 1.0, r2[4] = r2[5] = r2[7] = 0.0,
		r3[0] = mat->m30, r3[1] = mat->m31,
		r3[2] = mat->m32, r3[3] = mat->m33,
		r3[7] = 1.0, r3[4] = r3[5] = r3[6] = 0.0;

	/* choose pivot - or die */
	if (fabsf(r3[0]) > fabsf(r2[0]))
		SWAP_ROWS_FLOAT(r3, r2);

	if (fabsf(r2[0]) > fabsf(r1[0]))
		SWAP_ROWS_FLOAT(r2, r1);

	if (fabsf(r1[0]) > fabsf(r0[0]))
		SWAP_ROWS_FLOAT(r1, r0);

	if (0.0 == r0[0])
		return 0;

	/* eliminate first variable     */
	m1 = r1[0] / r0[0];
	m2 = r2[0] / r0[0];
	m3 = r3[0] / r0[0];
	s = r0[1];
	r1[1] -= m1 * s;
	r2[1] -= m2 * s;
	r3[1] -= m3 * s;
	s = r0[2];
	r1[2] -= m1 * s;
	r2[2] -= m2 * s;
	r3[2] -= m3 * s;
	s = r0[3];
	r1[3] -= m1 * s;
	r2[3] -= m2 * s;
	r3[3] -= m3 * s;
	s = r0[4];

	if (s != 0.0) {
		r1[4] -= m1 * s;
		r2[4] -= m2 * s;
		r3[4] -= m3 * s;
	}

	s = r0[5];
	if (s != 0.0) {
		r1[5] -= m1 * s;
		r2[5] -= m2 * s;
		r3[5] -= m3 * s;
	}

	s = r0[6];
	if (s != 0.0) {
		r1[6] -= m1 * s;
		r2[6] -= m2 * s;
		r3[6] -= m3 * s;
	}

	s = r0[7];
	if (s != 0.0) {
		r1[7] -= m1 * s;
		r2[7] -= m2 * s;
		r3[7] -= m3 * s;
	}

	/* choose pivot - or die */
	if (fabsf(r3[1]) > fabsf(r2[1]))
		SWAP_ROWS_FLOAT(r3, r2);
	if (fabsf(r2[1]) > fabsf(r1[1]))
		SWAP_ROWS_FLOAT(r2, r1);
	if (0.0 == r1[1])
		return 0;

	/* eliminate second variable */
	m2 = r2[1] / r1[1];
	m3 = r3[1] / r1[1];
	r2[2] -= m2 * r1[2];
	r3[2] -= m3 * r1[2];
	r2[3] -= m2 * r1[3];
	r3[3] -= m3 * r1[3];

	s = r1[4];
	if (0.0 != s) {
		r2[4] -= m2 * s;
		r3[4] -= m3 * s;
	}

	s = r1[5];
	if (0.0 != s) {
		r2[5] -= m2 * s;
		r3[5] -= m3 * s;
	}

	s = r1[6];
	if (0.0 != s) {
		r2[6] -= m2 * s;
		r3[6] -= m3 * s;
	}

	s = r1[7];
	if (0.0 != s) {
		r2[7] -= m2 * s;
		r3[7] -= m3 * s;
	}

	/* choose pivot - or die */
	if (fabsf(r3[2]) > fabsf(r2[2]))
		SWAP_ROWS_FLOAT(r3, r2);
	if (0.0 == r2[2])
		return 0;

	/* eliminate third variable */
	m3 = r3[2] / r2[2];
	r3[3] -= m3 * r2[3], r3[4] -= m3 * r2[4],
		r3[5] -= m3 * r2[5], r3[6] -= m3 * r2[6], r3[7] -= m3 * r2[7];

	/* last check */
	if (0.0 == r3[3])
		return 0;

	s = 1.0 / r3[3]; /* now back substitute row 3 */
	r3[4] *= s;
	r3[5] *= s;
	r3[6] *= s;
	r3[7] *= s;
	m2 = r2[3]; /* now back substitute row 2 */
	s = 1.0 / r2[2];
	r2[4] = s * (r2[4] - r3[4] * m2), r2[5] = s * (r2[5] - r3[5] * m2),
		r2[6] = s * (r2[6] - r3[6] * m2), r2[7] = s * (r2[7] - r3[7] * m2);
	m1 = r1[3];
	r1[4] -= r3[4] * m1, r1[5] -= r3[5] * m1,
		r1[6] -= r3[6] * m1, r1[7] -= r3[7] * m1;
	m0 = r0[3];
	r0[4] -= r3[4] * m0, r0[5] -= r3[5] * m0,
		r0[6] -= r3[6] * m0, r0[7] -= r3[7] * m0;
	m1 = r1[2]; /* now back substitute row 1 */
	s = 1.0 / r1[1];
	r1[4] = s * (r1[4] - r2[4] * m1), r1[5] = s * (r1[5] - r2[5] * m1),
		r1[6] = s * (r1[6] - r2[6] * m1), r1[7] = s * (r1[7] - r2[7] * m1);
	m0 = r0[2];
	r0[4] -= r2[4] * m0, r0[5] -= r2[5] * m0,
		r0[6] -= r2[6] * m0, r0[7] -= r2[7] * m0;
	m0 = r0[1]; /* now back substitute row 0 */
	s = 1.0 / r0[0];
	r0[4] = s * (r0[4] - r1[4] * m0), r0[5] = s * (r0[5] - r1[5] * m0),
		r0[6] = s * (r0[6] - r1[6] * m0), r0[7] = s * (r0[7] - r1[7] * m0);

	out->m00 = r0[4];
	out->m01 = r0[5], out->m02 = r0[6];
	out->m03 = r0[7], out->m10 = r1[4];
	out->m11 = r1[5], out->m12 = r1[6];
	out->m13 = r1[7], out->m20 = r2[4];
	out->m21 = r2[5], out->m22 = r2[6];
	out->m23 = r2[7], out->m30 = r3[4];
	out->m31 = r3[5], out->m32 = r3[6];
	out->m33 = r3[7];
	
	return 1;
}

ShaderData loadShader(const char* vertShaderPath, const char* fragShaderPath){
	ShaderData data;
	data.program = 0;
	
	createProgram(vertShaderPath, fragShaderPath, &data);

	return data;
}

GLuint createProgram(const char* vertShaderPath, const char* fragShaderPath, ShaderData *data) {
	GLuint program;
	GLuint vertShader, fragShader;
	
	vertShader = createShader(GL_VERTEX_SHADER, vertShaderPath);
	fragShader = createShader(GL_FRAGMENT_SHADER, fragShaderPath);
	
	if(vertShader == 0 || fragShader == 0) {
		return 0;
	}

	checkShader("vertex shader", vertShader);
	checkShader("fragment shader", fragShader);

	program = glCreateProgram();
	
	if(program == 0){
                printf("Failed to create program\n");
		deleteShaderObject(program, vertShader);
		deleteShaderObject(program, fragShader);
		return 0;
	}
	
	data->program = program;
	
	glAttachShader(program, vertShader);
	glAttachShader(program, fragShader);

	glLinkProgram(program);
	
	glBindAttribLocation(program, 1, "OBJ_Texcoord");
	
	int location0 = glGetUniformLocation(program, "Difftexture0");
	int location1 = glGetUniformLocation(program, "Difftexture1");
	data->modelMatrixLoc = glGetUniformLocation(program, "ModelMatrix");
	data->viewMatrixLoc = glGetUniformLocation(program, "ViewMatrix");
	data->projMatrixLoc = glGetUniformLocation(program, "ProjectionMatrix");

	setZeroMat4(&data->viewMatrix);
	setZeroMat4(&data->projMatrix);
	
	// Calculate initial projection based on defined values
	setProjectionMatrix(data, FOV, ZNEAR, ZFAR);
	
	glUseProgram(program);
	glUniform1i(location0, 0);
	glUniform1i(location1, 1);
	glUniformMatrix4fv(data->projMatrixLoc, 1, false, data->projMatrix.m);
	// Fill matrix data with zeros for now
	glUniformMatrix4fv(data->modelMatrixLoc, 1, false, data->viewMatrix.m);
	glUniformMatrix4fv(data->viewMatrixLoc, 1, false, data->viewMatrix.m);
	
	glUseProgram(0);
	
	deleteShaderObject(program, vertShader);
	deleteShaderObject(program, fragShader);
	
	return program;
}

void setViewMatrix(ShaderData *data, float cposX, float cposY, float cposZ, float crotX, float crotY, float crotZ){
	setIdentityMat4(&data->viewMatrix);
	rotateMat4(&data->viewMatrix, degToRad(crotX), 1, 0, 0);
	rotateMat4(&data->viewMatrix, degToRad(crotY), 0, 1, 0);
	rotateMat4(&data->viewMatrix, degToRad(crotZ), 0, 0, 1);
	translateMat4(&data->viewMatrix, -cposX, -cposY, -cposZ);
}

void setProjectionMatrix(ShaderData *data, float fov, float zNear, float zFar){
	float xymax = zNear * (float)(tan(degToRad(fov * 0.5f)));
	float ymin = -xymax;
	float xmin = -xymax;

	float width = xymax - xmin;
	float height = xymax - ymin;

	float depth = zFar - zNear;
	float q = -(zFar + zNear) / depth;
	float qn = -2.0f * (zFar * zNear) / depth;

	float w = 2.0f * (zNear / width);
	w = w / ((float)SCREEN_WIDTH/(float)SCREEN_HEIGHT);
	float h = 2.0f * zNear / height;
	
	setZeroMat4(&data->projMatrix);
	data->projMatrix.m00 = w;
	data->projMatrix.m11 = h;
	data->projMatrix.m22 = q;
	data->projMatrix.m23 = -1.0f;
	data->projMatrix.m32 = qn;
}

void setOrthoProjectionMatrix(ShaderData *data, float left, float right, float bottom, float top, float zNear, float zFar){
	float width = right - left;
	float height = top - bottom;
	float depth = zFar - zNear;
	
	setZeroMat4(&data->projMatrix);
	data->projMatrix.m00 = 2.0f/width;
	data->projMatrix.m30 = -((right+left)/width);
	data->projMatrix.m11 = 2.0f/height;
	data->projMatrix.m31 = -((top+bottom)/height);
	data->projMatrix.m22 = -2.0f/depth;
	data->projMatrix.m32 = -((zFar+zNear)/depth);
	data->projMatrix.m33 = 1.0f;
	//transposeMat4(&data->projMatrix);
}

void setModelMatrix(const Matrix4f *matrix, const ShaderData *data){
	glUniformMatrix4fv(data->modelMatrixLoc, 1, false, matrix->m);
}

int enableShader(const ShaderData *data){
	if(data->program < 1)
		return 0;
	
	glUseProgram(data->program);

	glUniformMatrix4fv(data->viewMatrixLoc, 1, false, data->viewMatrix.m);
	glUniformMatrix4fv(data->projMatrixLoc, 1, false, data->projMatrix.m);
	
	return 1;
}

void disableShader(){
	glUseProgram(0);
}

GLuint createShader(GLenum shaderType, const char* path){
	GLuint shader = glCreateShader(shaderType);
	if(shader == 0) {
                printf("Failed to create shader\n");
		return 0;
	}

	char *shaderCode = readFile(path);
	const char *constShaderCode = shaderCode;
	if(shaderCode == NULL){
		return 0;
	}

	glShaderSource(shader, 1, &constShaderCode, NULL);
	glCompileShader(shader);
	
	free(shaderCode);
	
	GLint shaderStatus;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &shaderStatus);

	printf("%s %d\n", path, shaderStatus);
	return shader;
}

void deleteShaderObject(GLuint program, GLuint shader){
	if(glIsShader(shader)){
		glDetachShader(program, shader);
		glDeleteShader(shader);
	}
}

void deleteProgram(ShaderData *data){
	if(glIsProgram(data->program)){
		glDeleteProgram(data->program);
	}
	
	data->program = 0;
	data->modelMatrixLoc = 0;
	data->viewMatrixLoc = 0;
	data->projMatrixLoc = 0;
	setZeroMat4(&data->viewMatrix);
	setZeroMat4(&data->projMatrix);
}

void checkShader(const char* what, GLuint shader) {
	GLint success = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

	if(success == GL_FALSE) {
		GLint maxLen = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLen);

		GLchar log[maxLen+1];
		glGetShaderInfoLog(shader, maxLen, NULL, log);

		printf("error in %s: %s\n", what, log);
	}
}

GLuint loadTexture(const char *filePath){
	SDL_Surface *img = IMG_Load(filePath);
	GLuint texture;
	
	if(img){
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);

		glTexImage2D(GL_TEXTURE_2D, 0, img->format->BytesPerPixel, img->w, img->h, 0, (img->format->BytesPerPixel == 3) ? GL_RGB : GL_RGBA, GL_UNSIGNED_BYTE, img->pixels);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

		SDL_FreeSurface(img);
		glBindTexture(GL_TEXTURE_2D, 0);
		
		return texture;
	}
	else{
		printf("Failed to load texture \"%s\"\n",filePath);
		return 0;
	}

}

inline float degToRad(float deg) {
    return deg*(M_PI/180.0f);
}

int createFramebuffer(FramebufferData *data){
	data->fbo = 0;
	data->rb = 0;
	data->texture = 0;
	data->texture2 = 0;
	data->depth = 0;
	
	glGenTextures(1, &data->texture);
	glBindTexture(GL_TEXTURE_2D, data->texture);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glGenTextures(1, &data->texture2);
	glBindTexture(GL_TEXTURE_2D, data->texture2);
	glTexImage2D(GL_TEXTURE_2D, 0, 3, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	glGenTextures(1, &data->depth);
	glBindTexture(GL_TEXTURE_2D, data->depth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SCREEN_WIDTH, SCREEN_HEIGHT, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D,0);
	
	glGenFramebuffersEXT(1, &data->fbo);
	glGenRenderbuffersEXT(1, &data->rb);
	
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, data->fbo);
	/*
	glBindRenderbuffer(GL_RENDERBUFFER_EXT, data->rb);
	
	glRenderbufferStorage(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT, SCREEN_WIDTH, SCREEN_HEIGHT);
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, data->rb);
	
	glBindRenderbuffer(GL_RENDERBUFFER_EXT, 0);
	*/
	
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, data->texture, 0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_TEXTURE_2D, data->depth, 0);
	
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	
	printf("fbo: %d, rb: %d, texture: %d\n",data->fbo,data->rb,data->texture);
	return 1;
}
