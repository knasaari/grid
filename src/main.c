#include <GL/glew.h>
#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <stdio.h>
#include "gl.h"
#include "types.h"

#include "primitives.h"
#include "grid.h"
#include "selection.h"
#include <time.h>
#include <math.h>

#define WORLD_WIDTH 20
#define WORLD_HEIGHT 2
#define GRID_SIZE 16

bool init(void);
void render(void);
void renderVBO(void);
void renderSelection(void);
void processMouse(const Matrix4f *modelMatrix);

//shader program, frag and vert
ShaderData shader, quadShader, quadShader2, postShader;
GLuint texture;
FramebufferData fbo;
Matrix4f postModelMatrix, postViewMatrix;

Grid grid;
Block ***grid_data;
VBO vbo, vbo_sel;
UV4 *uvs;

int frameCount = 0;
float camrX = 25.0f, camrY = 0, camrZ = 0;
float zoom = 0.25f, selectionX = 0.0f, selectionY = 17.0f, selectionZ = 0.0f;
int getUnproject = 0, mouseX, mouseY;

int main(int argc, char **argv) {
	srand(time(NULL));
	bool run = true;

	if (init() == false) {
		printf("gl failed\n");
		return 0;
	}

	texture = loadTexture("./32x32terrain.png");
	shader = loadShader(VERT_SHADER, FRAG_SHADER);
	quadShader = loadShader("./quad.vert", "./quad.frag");
	quadShader2 = loadShader("./quad2.vert", "./quad.frag");
	postShader = loadShader("./post.vert", "./post.frag");
	createFramebuffer(&fbo);
	
	setViewMatrix(&postShader, 0, 0, 15.0f, 0, 0, 0);
	//setOrthoProjectionMatrix(&shader, 0, ZNEAR, ZFAR);
	setOrthoProjectionMatrix(&postShader, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, ZNEAR, 20.0f);
	
	setIdentityMat4(&postModelMatrix);
	translateMat4(&postModelMatrix, 0, 0, 0);
	rotateMat4(&postModelMatrix, 0, 1, 0, 0);
	rotateMat4(&postModelMatrix, 0, 0, 1, 0);
	rotateMat4(&postModelMatrix, 0, 0, 0, 1);
	
	
	SDL_Event event;

	initUV4(&uvs, 0);
	initGrid(&grid, &grid_data, WORLD_WIDTH, WORLD_HEIGHT, GRID_SIZE);
	generateGrid(&grid, &grid_data);
	createVBO(&grid, &vbo);
	updateVBO(&grid, grid_data, &vbo, uvs);

	// createSelection(&vbo_sel, uvs);

	while (run) {

		while (SDL_PollEvent(&event)) {
			if (event.type == SDL_QUIT || event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE)
				run = false;

			else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_w)
				zoom -= 0.025f;
			else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_s)
				zoom += 0.025f;
			else if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_F5) {
				deleteProgram(&shader);
				shader = loadShader(VERT_SHADER, FRAG_SHADER);
				
				deleteProgram(&postShader);
				postShader = loadShader("./post.vert", "./post.frag");
				setViewMatrix(&postShader, 0, 0, 15.0f, 0, 0, 0);
				setOrthoProjectionMatrix(&postShader, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, ZNEAR, 20.0f);
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT) {
				mouseX = event.button.x;
				mouseY = event.button.y;
				getUnproject = 1;
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_RIGHT) {
				mouseX = event.button.x;
				mouseY = event.button.y;
				getUnproject = 2;
			}
		}
		camrY += 0.25f;

		frameCount++;
		render();
	}

	return 0;
}

bool init(void) {
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return false;

#ifdef __WIN32__
	freopen("CON", "w", stdout);
	freopen("CON", "w", stderr);
#endif
	if (SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 32, SDL_OPENGL) < 0)
		return false;

	if (!initGL())
		return false;

	return true;
}

void render(void) {
	// camera positioning
	setViewMatrix(&shader, 0, grid.width * 0.2f * zoom, grid.width * 1.0f * zoom, camrX, camrY, camrZ);

	// Calculate modelMatrix
	Matrix4f modelMatrix;
	setIdentityMat4(&modelMatrix);
	translateMat4(&modelMatrix, 0, 0, 0);
	rotateMat4(&modelMatrix, 0, 1, 0, 0);
	rotateMat4(&modelMatrix, M_PI, 0, 1, 0);
	rotateMat4(&modelMatrix, 0, 0, 0, 1);
	translateMat4(&modelMatrix, -grid.width * 0.5f, 0.0f, -grid.width * 0.5f);
	/*
	
	 */

	//
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo.fbo);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (enableShader(&shader)) {
		setModelMatrix(&modelMatrix, &shader);
		renderVBO();
		disableShader();
	}
	processMouse(&modelMatrix);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
	
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (enableShader(&postShader)) {
		setModelMatrix(&postModelMatrix, &postShader);
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, fbo.texture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, fbo.depth);
		
		glBegin(GL_TRIANGLES),
		glTexCoord2f(0, 1);
		glVertex2f(0, 0);
		glTexCoord2f(0, 0);
		glVertex2f(0, SCREEN_HEIGHT);
		glTexCoord2f(1, 0);
		glVertex2f(SCREEN_WIDTH, SCREEN_HEIGHT);

		glTexCoord2f(0, 1);
		glVertex2f(0, 0);
		glTexCoord2f(1, 0);
		glVertex2f(SCREEN_WIDTH, SCREEN_HEIGHT);
		glTexCoord2f(1, 1);
		glVertex2f(SCREEN_WIDTH, 0);
		glEnd();

		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, 0);
		
		disableShader();
	}

	SDL_GL_SwapBuffers();
}

void processMouse(const Matrix4f *modelMatrix){
	if (getUnproject > 0) {
		float sel[3];
		Matrix4f mview,rot;
		mulMat4(&shader.viewMatrix, modelMatrix, &mview);
		glhUnProjectf(mouseX, mouseY, &mview, &shader.projMatrix, sel);
		float left[4], dest[4];
		left[0] = 0;
		left[1] = 0;
		left[2] = -0.5f;
		left[3] = 0;
		setIdentityMat4(&rot);
		
		rotateMat4(&rot, degToRad(-camrY), 0, 1, 0);
		rotateMat4(&rot, degToRad(-camrX), 1, 0, 0);
		//rotateMat4(&rot, degToRad(0), 0, 0, 1);
		transformMat4(&rot, left, dest);
		printf("%f %f %f %f\n",dest[0],dest[1],dest[2],dest[3]);
		sel[0] = roundf(sel[0]+dest[0]);
		sel[1] = roundf(sel[1]);
		sel[2] = roundf(sel[2]+dest[2]);
		
		
		
		int blockX = sel[0];
		int blockY = sel[1];
		int blockZ = sel[2];

		if (getUnproject == 1) {
			if (setBlock(&grid, grid_data, blockX, blockY, blockZ, 0)) {
				updateChunk(&grid, grid_data, &vbo, uvs, whichChunk(&grid, blockX, blockY, blockZ));
			}
		}
		else if (getUnproject == 2) {
			if (setBlock(&grid, grid_data, blockX, blockY, blockZ, 1)) {
				updateChunk(&grid, grid_data, &vbo, uvs, whichChunk(&grid, blockX, blockY, blockZ));
			}
		}
		getUnproject = 0;
	}
}

void setColor(int i) {
	switch (i) {
		case 0:
			glColor3f(194.0 / 255.0, 59.0 / 255.0, 34.0 / 255.0); // red
			break;
		case 1:
			glColor3f(3.0 / 255.0, 192.0 / 255.0, 60.0 / 255.0); // green
			break;
		case 2:
			glColor3f(119.0 / 255.0, 158.0 / 255.0, 203.0 / 255.0); // blue
			break;
		case 3:
			glColor3f(255.0 / 255.0, 179.0 / 255.0, 71.0 / 255.0); // orange
			break;
		case 4:
			glColor3f(150.0 / 255.0, 111.0 / 255.0, 214.0 / 255.0); // purple
			break;
		case 5:
			glColor3f(207.0 / 255.0, 207.0 / 255.0, 196.0 / 255.0); // grey
			break;
		case 6:
			glColor3f(130.0 / 255.0, 105.0 / 255.0, 83.0 / 255.0); // brown
			break;
		case 7:
			glColor3f(253.0 / 255.0, 253.0 / 255.0, 150.0 / 255.0);
			break;
		default:
			glColor3f(207.0 / 255.0, 207.0 / 255.0, 196.0 / 255.0);
	};
}

void renderVBO(void) {
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo.id);
	glBindTexture(GL_TEXTURE_2D, texture);

	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof (GLfloat)*5,
		(void*) 0
		);
	glVertexAttribPointer(
		1,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof (GLfloat)*5,
		(void*) (sizeof (GLfloat)*3)
		);
	int chunkCount = grid.worldWidth * grid.worldHeight * grid.worldWidth;
	int oneChunkMaxSize = vbo.indexCount / chunkCount;

	int i;
	for (i = 0; i < chunkCount; i++) {
		glColor3f(0.1, 0.4, 0.1);
		glDrawArrays(GL_TRIANGLES, oneChunkMaxSize*i, vbo.chunkSizes[i]);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

void renderSelection(void) {
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sel.id);
	glBindTexture(GL_TEXTURE_2D, texture);

	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof (GLfloat)*5,
		(void*) 0
		);
	glVertexAttribPointer(
		1,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof (GLfloat)*5,
		(void*) (sizeof (GLfloat)*3)
		);

	glDrawArrays(GL_TRIANGLES, 0, vbo_sel.indexCount);

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}
