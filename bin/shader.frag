#version 120

uniform sampler2D Difftexture0;
varying vec4 verpos;
varying vec2 texcoord;

void main(void){
	gl_FragColor = texture2D(Difftexture0, texcoord);
}

void depthTexturing(){
	//gl_FragColor = gl_Color;
	// gl_FragColor = vec4(1.0,1.0,1.0,1.0);
	float contrast = 0.9;
	float brightness = 15;
	float depthBrightness = clamp(gl_FragCoord.w*5,0,1);
	vec4 color = vec4(vec3(depthBrightness),1);
	color = (color - vec4(0.5,0.5,0.5,0) * max(contrast,0)) + vec4(0.5,0.5,0.5,0);
	//color *= brightness;
	color *= texture2D(Difftexture0, texcoord);
	//color = clamp(color,0.1,1);
	//float co = verpos.y/10;
	//gl_FragColor = vec4(co,co,1-co,1.0)*gl_Color*clamp(gl_FragCoord.w*200,0.1,1.3);
	gl_FragColor = color;
}