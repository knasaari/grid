#version 120

attribute vec2 OBJ_Texcoord;

varying vec2 texcoord;
varying vec2 v_blurTexCoords[14];

void main(void){
	texcoord =  gl_MultiTexCoord0.xy;
	
	v_blurTexCoords[ 0] = texcoord + vec2(-0.028, 0.0);
    v_blurTexCoords[ 1] = texcoord + vec2(-0.024, 0.0);
    v_blurTexCoords[ 2] = texcoord + vec2(-0.020, 0.0);
    v_blurTexCoords[ 3] = texcoord + vec2(-0.016, 0.0);
    v_blurTexCoords[ 4] = texcoord + vec2(-0.012, 0.0);
    v_blurTexCoords[ 5] = texcoord + vec2(-0.008, 0.0);
    v_blurTexCoords[ 6] = texcoord + vec2(-0.004, 0.0);
    v_blurTexCoords[ 7] = texcoord + vec2( 0.004, 0.0);
    v_blurTexCoords[ 8] = texcoord + vec2( 0.008, 0.0);
    v_blurTexCoords[ 9] = texcoord + vec2( 0.012, 0.0);
    v_blurTexCoords[10] = texcoord + vec2( 0.016, 0.0);
    v_blurTexCoords[11] = texcoord + vec2( 0.020, 0.0);
    v_blurTexCoords[12] = texcoord + vec2( 0.024, 0.0);
    v_blurTexCoords[13] = texcoord + vec2( 0.028, 0.0);
	
	vec4 v = vec4(gl_Vertex.xyz,1);
	gl_Position = gl_ModelViewProjectionMatrix * v;
}
