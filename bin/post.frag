#version 120

uniform sampler2D Difftexture0;
uniform sampler2D Difftexture1;
varying vec2 texcoord;
varying vec2 v_blurTexCoords[14];

float linearizeDepth(vec2 uv){
  float n = 0.1; // camera z near
  float f = 90.0; // camera z far
  float z = texture2D(Difftexture1, uv).x;
  return (2.0 * n) / (f + n - z * (f - n));	
}

float lumRGB(vec3 v){   
        return dot(v, vec3(0.212, 0.716, 0.072));       
}  
  
void fxaa( void )
{
    vec2 buffersize = vec2(1280,800);
    float   inverse_buffer_size = (1.0 / buffersize.x, 1.0 / buffersize.y);
    vec2    UV = gl_FragCoord.xy / buffersize;
 
    float   w = 1.75;
 
    float t = lumRGB(texture2D(Difftexture0, UV + vec2(0.0, -1.0) * w * inverse_buffer_size).xyz);
        float l = lumRGB(texture2D(Difftexture0, UV + vec2(-1.0, 0.0) * w * inverse_buffer_size).xyz);
        float r = lumRGB(texture2D(Difftexture0, UV + vec2(1.0, 0.0) * w * inverse_buffer_size).xyz);
        float c = lumRGB(texture2D(Difftexture0, UV + vec2(0.0, 1.0) * w * inverse_buffer_size).xyz);
 
    vec2    n = vec2(-(t - c), r - l);
    float   nl = length(n);
 
    if  (nl < (1.0 / 6.0))
        gl_FragColor = texture2D(Difftexture0, UV);
 
    else
    {
        n *= inverse_buffer_size / nl;
 
        vec4 o = texture2D(Difftexture0, UV);
        vec4 t0 = texture2D(Difftexture0, UV + n * 0.5) * 0.9;
        vec4 t1 = texture2D(Difftexture0, UV - n * 0.5) * 0.9;
        vec4 t2 = texture2D(Difftexture0, UV + n) * 0.75;
        vec4 t3 = texture2D(Difftexture0, UV - n) * 0.75;
 
        gl_FragColor = (o + t0 + t1 + t2 + t3) / 4.3;
    }
        
        
}

vec4 hBlur(){
	vec4 blur = vec4(0.0);
    blur += texture2D(Difftexture0, v_blurTexCoords[0])*0.0044299121055113265;
    blur += texture2D(Difftexture0, v_blurTexCoords[1])*0.00895781211794;
    blur += texture2D(Difftexture0, v_blurTexCoords[2])*0.0215963866053;
    blur += texture2D(Difftexture0, v_blurTexCoords[3])*0.0443683338718;
    blur += texture2D(Difftexture0, v_blurTexCoords[4])*0.0776744219933;
    blur += texture2D(Difftexture0, v_blurTexCoords[5])*0.115876621105;
    blur += texture2D(Difftexture0, v_blurTexCoords[6])*0.147308056121;
    blur += texture2D(Difftexture0, texcoord) * 0.159576912161;
    blur += texture2D(Difftexture0, v_blurTexCoords[7])*0.147308056121;
    blur += texture2D(Difftexture0, v_blurTexCoords[8])*0.115876621105;
    blur += texture2D(Difftexture0, v_blurTexCoords[9])*0.0776744219933;
    blur += texture2D(Difftexture0, v_blurTexCoords[10])*0.0443683338718;
    blur += texture2D(Difftexture0, v_blurTexCoords[11])*0.0215963866053;
    blur += texture2D(Difftexture0, v_blurTexCoords[12])*0.00895781211794;
    blur += texture2D(Difftexture0, v_blurTexCoords[13])*0.0044299121055113265;
	return blur;
}

void main(void){
//	fxaa();
//	return;
	
	
	float d = linearizeDepth(texcoord);
	d = 1-d;
	vec4 depth = vec4(d,d,d,1);
	//vec2 texc = vec2(gl_FragCoord.x/1280,gl_FragCoord.y/800);
	vec4 color = texture2D(Difftexture0, texcoord);
	gl_FragColor = color;
	//gl_FragColor = texture2D(Difftexture0, texcoord)*depth;
}