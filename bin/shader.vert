#version 120

uniform mat4 ModelMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ProjectionMatrix;

attribute vec2 OBJ_Texcoord;
varying vec4 verpos;
varying vec2 texcoord;

void main(void){
	gl_FrontColor = gl_Color;
	
	verpos = vec4(gl_Vertex);
	texcoord = OBJ_Texcoord;
	
	vec4 v = ProjectionMatrix * ViewMatrix * ModelMatrix * vec4(gl_Vertex.xyz,1);
	//texcoord = vec2(v.x/256+0.5,v.y/256+0.5);
	gl_Position = v;
}
